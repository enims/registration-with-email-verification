<?php

// REGISTRATION VERIFICATION EMAIL
function verify_email($the_id) {

    // THIS SETS THE GLOBALS SO YOU CAN USE VARIABLES IN THE FUNCTION
    global $sett_reply_email;
    global $dbs;
    global $sett_title;

    foreach($dbs->query("SELECT * FROM your_user_table WHERE your_id_field='{$the_id}'") as $row) {}
    // SETUP ALL THE NEEDED VARIABLES FROM THE USERS PROFILE
    $id = $row['id'];
    $email = stripslashes($row['email']);

    $to = "{$email}";
    $from = "NoReply <{$sett_reply_email}>";
    // SET THE SUBJECT FOR THE EMAIL
    $subject = "your_website Signup Verification Email";
    // CONSTRUCT THE HTML EMAIL AND SEND
    $html = "<html><head>
    <style>
    a { text-align:center; color:#ffffff; padding:5px 10px; background-color:#333333; border-radius:5px 5px; border:1px solid #cccccc; font-size:14pt; cursor:pointer; text-decoration:none; }
    a:hover { background-color:#3a4e3f; }
    </style>
    </head>
    <body>
    <br />
    <table align=\"center\" width=\"666px\" border=\"0\" cellspacing=\"1\" cellpadding=\"1\">
    <tr>
    <td align=\"center\" width=\"100%\" style=\"font-size:22pt;\">
    Welcome to {$sett_title}
    </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
    <td>
    We have received a signup with the following information. In order to login and use the system, you'll need to verify the information by clicking
    on the lick provided below.<br />
    Email Address/Username: {$email}<br />
    Password: Password you used during registration<br />
    <br />
    <a href=\"http://yourwebsite.com/register.php?kladgh2lkjfdaihef9={$id}&d;lgj;l43;l;80plm3;l5mkj;ljkm#kljlh^lkfkh\">Verify Now</a><br />
    <br />
    </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    </table>
    <br />
    </body></html>";
    $htmlmessagebody = wordwrap($html, 60);
    $boundary = md5(time());
    $headers = "MIME-Version: 1.0\r\n" .
        "Content-type: multipart/mixed; boundary=\"$boundary\"\r\n" .
        "From: " . $from . "\r\n";
    $message = "--" . $boundary . "\n" .
        "Content-Type: text/html; charset=iso-8859-1\n" .
        "Content-Transfer-Encoding: 7bit\n\n" .
        $htmlmessagebody . "\n" .
        "--" . $boundary . "--";
    $mailsend = mail($to, $subject, $message, $headers);
}

// ERROR MESSAGE FUNCTION GOOD
function error_message_good($errormsg, $redir, $redir_to, $pause_time) {
    echo "<div id=\"content_wrapper\">
        <div id=\"error_content_good\">
            {$errormsg}
        </div>
    </div>";
    if($redir == '1') {
        echo "<meta http-equiv=\"refresh\" content=\"$pause_time;url=".$redir_to."\">";
    }
}
// ERROR MESSAGE FUNCTION BAD
function error_message_bad($errormsg, $redir, $redir_to, $pause_time) {
    echo "<div id=\"content_wrapper\">
        <div id=\"error_content_bad\">
            {$errormsg}
        </div>
    </div>";
    if($redir == '1') {
        echo "<meta http-equiv=\"refresh\" content=\"$pause_time;url=".$redir_to."\">";
    }
}

?>