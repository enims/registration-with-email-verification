<?php

// CHECK TO SEE IF THE REGISTER BUTTON WAS PRESSED. STORES THE POST DATA FOR THE FORM
if(isset($_POST['register'])) {
    $email = $_POST['email'];
    $password = $_POST['password'];
} else {
    $email = "";
    $password = "";
}

// BUILD OUT THE HTML FORM
echo "<div style=\"height:20px;\"></div>
<h1>REGISTER FOR your_website_name</h1>
<div style=\"height:20px;\"></div>
<form action=\"{$_SERVER['PHP_SELF']}\" method=\"post\" enctype=\"multipart/form-data\">
    <input type=\"hidden\" name=\"code\" value=\"\"/>
    <div class=\"form_text\">
        Email Address
    </div>
    <div class=\"clear_pad\"></div>
    <div>
        <input class=\"textbox\" type=\"text\" style=\"width:100%\" name=\"email\" maxlength=\"100\" value=\"{$email}\" />
    </div>
    <div class=\"clear_pad\"></div>
    <div class=\"form_text\">
        Password
    </div>
    <div class=\"clear_pad\"></div>
    <div>
        <input class=\"textbox\" type=\"text\" style=\"width:100%\" name=\"password\" maxlength=\"20\" value=\"{$password}\" />
    </div>
    <div class=\"clear_pad\"></div>
    <div>
        <input class=\"submit\" type=\"submit\" name=\"register\" value=\"REGISTER NOW\" />
    </div>
</form>";

?>