<?php

// THIS STARTS THE SESSIONS ON THE PAGE SO YOU CAN PASS SESSION VARS FROM PAGE TO PAGE
session_start();
// THIS IS THE HONEYPOT SESSION VAR - IT NEEDS TO BE EMPTY TO WORK AS A BOT WILL ATTEMPT TO FILL IT OUT
$the_code = $_POST['CODE'];
// THIS SETS THE CURRENT DATE AND TIME
$today_full = date("Y-m-d H:i:s");
// THIS IS THE ID PULLED FROM THE LINK IN THE EMAIL
$the_verify_id = $_GET['kladgh2lkjfdaihef9'];

// THIS GRABS THE HEADER WITH YOUR PDO DB CONNECTION, METADATA, GET STYLESHEET, ETC
include("header.php");
// THIS GRABS THE FUNCTIONS FOR THE EMAIL AND THE MESSAGES - GOOD AND BAD
include("functions.php");

// IF THE REGISTER BUTTON WAS PRESSED
if(isset($_POST['register'])) {
    // CHECK IF THE REQUIRED EMAIL IS FILLED IN AND FORMATTED PROPERLY
    if(empty($_POST['email'])) {
        // EMPTY FIELD
        $email = FALSE;
        error_message_bad("Please enter an email address.", "", "", "");
    } else {
        // FOUND IT BUT IS IT FORMATTED PROPERLY?
        if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $email = FALSE;
            error_message_bad("You have entered an invalid email address (i.e. xxx@xxx.xxx)", "", "", "");
        } else {
            // LOOKS GOOD BUT IS IT IN THE SYSTEM ALREADY?
            $sql = "SELECT email FROM your_user_table WHERE your_email_field='{$_POST['email']}'";
            $stmt = $dbs->prepare($sql);
            $stmt->execute();
            if($stmt->rowCount() != 0) {
                // OOPS, IT EXISTS
                $email = FALSE;
                error_message_bad("That email address is already in our system.", "", "", "");
            } else {
                // GOOD TO GO
                $email = addslashes($_POST['email']);
            }
        }
    }
    // CHECK IF THE REQUIRED PASSWORD IS FILLED IN
    if(empty($_POST['password'])) {
        // EMPTY FIELD
        $password = FALSE;
        error_message_bad("Please enter a password.", "", "", "");
    } else {
        // GOOD TO GO
        $password = addslashes($_POST['password']);
    }
    // CHECK IF A ROBOT FILLED IN THE HONEYPOT FIELD
    if($_POST['code'] != "") {
        // THEY DID
        $code_good = FALSE;
        error_message_bad("Damn you robot!", "", "", "");
    } else {
        // GOOD TO GO
        $code_good = TRUE;
    }
    // CHECK IF ALL REQUIRED FIELDS ARE GOOD TO GO
    if($email && $password && $code_good) {
        $sql = "INSERT INTO your_user_table (username, password, email, level, active) VALUES ('{$email}', MD5('{$password}'),
        '{$email}', 'member', 'N')";
        $stmt = $dbs->prepare($sql);
        $stmt->execute();
        if($stmt->rowCount() != 0) {
            // GET THE LAST ID INSERTED INTO THE DB
            $the_latest_id = $dbs->lastInsertId();
            // SEND OU THE EMAIL TO THE NEW USER
            verify_email($the_latest_id);
            error_message_good("Added! Please check your email for the account verification link.", "1", "/", "3");
        } else {
            error_message_bad("There was an error. Please contact your administrator.", "", "", "");
        }
    }
    // INCLUDE THE REGISTRATION CONTENT PAGE IN CASE THEY NEEDED TO FILL SOMETHING IN
    include("includes/register_content.php");
    
// IF THEY ARE TRYING TO ACTIVATE THEIR ACCOUNT    
} elseif($the_verify_id != '') {
    // CHECK TO SEE IF THEIR ID IS NUMERIC
    if(is_numeric($the_verify_id)) {
        // LOOKS GOOD SO UPDATE THE USER TABLE FLAG
        $sql = "UPDATE your_user_table SET active='Y' WHERE your_id_field='{$the_verify_id}'";
        $stmt = $dbs->prepare($sql);
        $stmt->execute();
        if($stmt->rowCount() != 0) {
            error_message_good("Thank you for taking the time to verify your new account.", "1", "login.php", "2");
        } else {
            error_message_bad("There was an error. Please contact your administrator.", "", "", "");
        }
    } else {
        error_message_bad("You have seemed to have gotten here by mistake.", "1", "/", "1");
    }
} else {
    // THIS IS THE DEFAULT PAGE CONTENT IF THEY WENT HERE FOR THE FIRST TIME
    include("includes/register_content.php");
}

// THIS GRABS THE FOOTER WITH YOUR BOTTOM LINKS, COPYRIGHT INFO, ETC
include("footer.php");

?>